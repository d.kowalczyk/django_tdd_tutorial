from .base import FunctionalTest




class MyListsTest(FunctionalTest):

    def test_logged_in_users_lists_are_saved_as_my_lists(self):
        # Edyta jest zalogowanym użytkownikiem
        self.create_pre_authenticated_session('edith@example.com')

        # Przechodzi do strony domowej i zaczyna nową listę
        self.browser.get(self.live_server_url)
        self.add_list_item('Retiulate splines')
        self.add_list_item('Immanentize eschaton')
        first_list_url = self.browser.current_url

        # Edyta dostrzegła link do "My lists" pierwszy raz
        self.browser.find_element_by_link_text('My lists').click()

        # Widzi, że jest tam jej lista nazwana tak jak pierwszy element listy
        self.wait_for(lambda: self.browser.find_element_by_link_text('Retiulate splines'))
        self.browser.find_element_by_link_text('Retiulate splines').click()
        self.wait_for(lambda: self.assertEqual(self.browser.current_url, first_list_url))
        
        # Postanowiła stworzyć nową listę
        self.browser.get(self.live_server_url)
        self.add_list_item('Click cows')
        second_list_url = self.browser.current_url

        # Pod zakladka My Lists jest jej nowa lista
        self.browser.find_element_by_link_text('My lists').click()
        self.wait_for(lambda: self.browser.find_element_by_link_text('Click cows'))
        self.browser.find_element_by_link_text('Click cows').click()
        self.wait_for(lambda: self.assertEqual(self.browser.current_url, second_list_url))

        # Wylogowywuje się, opcja my list znikła
        self.browser.find_element_by_link_text('Log out').click()
        self.wait_for(lambda: self.assertEqual(self.browser.find_elements_by_link_text('My lists'),[]))


        