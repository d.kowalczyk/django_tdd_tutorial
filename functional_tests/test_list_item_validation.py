from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys

class ItemValidationTest(FunctionalTest):

    def get_error_element(self):
        return self.browser.find_element_by_css_selector('.has-error')

    def test_cannot_add_empty_list_items(self):
        # Edyta przechodzi do strony głównej i chce dodać pusty przedmiot do listy.
        # Wciska enter z pustym input boxem
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys(Keys.ENTER)
        
        # Strona odświeża się i wyskakuje komunikat,że przedmiot musi mieć nazwę
        self.wait_for(lambda: 
            self.browser.find_element_by_css_selector(
                '#id_text:invalid'
        ))

        # Edyta jeszcze raz prubuje dodać element tym razem z nazwą i tym razem się udaje
        self.get_item_input_box().send_keys('Buy milk')
        self.wait_for(lambda:
            self.browser.find_element_by_css_selector(
                '#id_text:valid'
        ))

        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy milk')

        # Edyta prubuje znow dodać pusty element
        self.get_item_input_box().send_keys(Keys.ENTER)

        # Ale znów wyskakuje komunikat o tym, że przedmiot musi mieć nazwę
        self.wait_for_row_in_list_table('1: Buy milk')
        self.wait_for(lambda: 
            self.browser.find_element_by_css_selector(
                '#id_text:invalid'
        ))

        # Może to poprawić dodając nazwe przedmiotu
        self.get_item_input_box().send_keys('Make tea')
        self.wait_for(lambda: 
        self.browser.find_element_by_css_selector(
            '#id_text:valid'
        ))     
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy milk')
        self.wait_for_row_in_list_table('2: Make tea')

    def test_cannot_add_duplicate_items(self):
        # Edyta przechodzi na stronę główną i tworzy nową listę
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys('Buy wellies')
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy wellies')

        # Przypadkowo prubuje dodać zduplikowany przedmiot
        self.get_item_input_box().send_keys('Buy wellies')
        self.get_item_input_box().send_keys(Keys.ENTER)
        
        # Ale wyskakuje pomocny komunikat o błędzie
        self.wait_for(lambda: self.assertEqual(
            self.get_error_element().text,
            "You've already got this in your list"
        ))

    def test_error_messages_are_cleared_on_input(self):
        # Edyta tworzy nową listę i dostaje błąd validacji
        self.browser.get(self.live_server_url)
        self.get_item_input_box().send_keys('Bander too thick')
        self.get_item_input_box().send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Bander too thick')
        self.get_item_input_box().send_keys('Bander too thick')
        self.get_item_input_box().send_keys(Keys.ENTER)

        self.wait_for(lambda: self.assertTrue(
            self.get_error_element().is_displayed()
        ))

        # Edyta zaczyna pisać w polu by pozbyć się błędu
        self.get_item_input_box().send_keys('a')

        # Error is disappears 
        self.wait_for(lambda: self.assertFalse(
            self.get_error_element().is_displayed()
        ))
