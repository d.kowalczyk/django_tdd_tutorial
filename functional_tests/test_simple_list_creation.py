from selenium.webdriver.common.keys import Keys
from .base import FunctionalTest
from selenium import webdriver

class NewVisitorTest(FunctionalTest):

    def test_can_start_a_list_for_one_user(self):
        
        self.browser.get(self.live_server_url)

        self.assertIn('To-Do', self.browser.title)
        header_text =  self.browser.find_element_by_tag_name('h1').text
        self.assertIn('To-Do',header_text)

        # Add first item
        inputbox = self.get_item_input_box()
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'Enter a to-do item'
        )        
        inputbox.send_keys('Buy peacock feathers')
        inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: Buy peacock feathers')

        # Add second item

        inputbox = self.get_item_input_box()
        inputbox.send_keys('Use peacock feathers to make a fly')
        inputbox.send_keys(Keys.ENTER)

        self.wait_for_row_in_list_table('1: Buy peacock feathers')
        self.wait_for_row_in_list_table('2: Use peacock feathers to make a fly')

        # self.fail('Finish the test!')

    def test_multiple_users_can_start_lists_at_different_urls(self):
        #Edyta tworzy nową listę to-do
        self.browser.get(self.live_server_url)
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Buy peacock feathers')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy peacock feathers')

        # Zauważyła, że jej lista ma unikatowy adres URL
        edith_list_url = self.browser.current_url
        self.assertRegex(edith_list_url, '/lists/.+')

        # Teraz nowy użytkownik Franek wchodzi na stronę
        ## używamy nowej przegladarki by upewnić się, że nie ma na niej informacji
        self.browser.quit()
        self.browser = webdriver.Firefox()
        
        # Franek odwiedza stronę, nie ma informacji Edyty
        self.browser.get(self.live_server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy peacock feathers', page_text)
        self.assertNotIn('make a fly', page_text)

        # Franek tworzy nową listę
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Buy Milk')
        inputbox.send_keys(Keys.ENTER)
        self.wait_for_row_in_list_table('1: Buy Milk')

        #Franek dostaje unikatowy adres URL listy
        francis_list_url = self.browser.current_url
        self.assertRegex(francis_list_url, '/lists/.+')
        self.assertNotEqual(francis_list_url,edith_list_url)

        # i znowu nie ma danych z listy Edyty
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy peacock feathers', page_text)
        self.assertIn('Buy Milk', page_text)
