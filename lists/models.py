from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
# Create your models here.

class List(models.Model):


    def get_absolute_url(self):
        return self.item_set.first().text

    @staticmethod
    def create_new(first_item_text, owner=None):
        list_ = List.objects.create(owner=owner)
        Item.objects.create(text=first_item_text, list=list_)
        return list_

class Item(models.Model):
    text = models.TextField(default='')
    list = models.ForeignKey(List,on_delete=models.CASCADE, default=None)

    class Meta:
        ordering = ('id',)
        unique_together = ('list', 'text')

    def __str__(self):
        return self.text
